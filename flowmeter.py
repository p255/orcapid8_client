import u3

class LabJack:
    def __init__(self):
        self.d = u3.U3()

class FlowMeter:
    def __init__(self, lj, max_flow=200, dac=0, flow_ain=0, ref_ain=2):
        self.d = lj.d
        self.max_flow = max_flow
        self.dac = dac
        self.flow_ain = flow_ain
        self.ref_ain = ref_ain
        
    def read_ain(self,ch):
        ainbits, = self.d.getFeedback(u3.AIN(ch))
        ainValue = self.d.binaryToCalibratedAnalogVoltage(ainbits, isLowVoltage = False, channelNumber = ch)
        return (ainValue)
    
    def write_dac(self,ch,val):
        if ch != 0 and ch != 1:
            print('must be DAC0 or DAC1')
            return
        DAC_VALUE = self.d.voltageToDACBits(val, dacNumber =ch, is16Bits = False)
        if ch == 0:
            self.d.getFeedback(u3.DAC0_8(DAC_VALUE))
        else:
            self.d.getFeedback(u3.DAC1_8(DAC_VALUE))
        

    # 
    def read_flow(self):
        ref = self.read_ain(self.ref_ain)
        flow = self.read_ain(self.flow_ain)
        return flow/ref*self.max_flow
    
    def set_flow(self,target):
        ref = self.read_ain(self.ref_ain)
        if ref < 4 or ref > 5.5:
            print('error! ref voltage was %0.3f. Aborting' % ref)
            return
        target_v = max(min(ref*target/self.max_flow,5),0)
        self.write_dac(self.dac,target_v)