import state
import numpy as np
import datetime
import os
import fileio

class Datum:
    def __init__(self, num_ch=8):
        self.time = 0
        self.TCA_int = np.zeros(num_ch,dtype=np.float32)
        self.TCB_int = np.zeros(num_ch,dtype=np.float32)
        self.TCA_ext = np.zeros(num_ch,dtype=np.float32)
        self.TCB_ext = np.zeros(num_ch,dtype=np.float32)
        self.TCA_fault = np.zeros(num_ch,dtype=np.int8)
        self.TCB_fault = np.zeros(num_ch,dtype=np.int8)
        self.Kp = np.zeros(num_ch,dtype=np.float16)
        self.Ki = np.zeros(num_ch,dtype=np.float16)
        self.Kd = np.zeros(num_ch,dtype=np.float16)
        self.output = np.zeros(num_ch)
        self.setpoint = np.zeros(num_ch)

class Logger:
    def __init__(self, num_ch=8, steps_per_save=100, data_folder_name="data", data_file_prefix="d"):
        self.data = []
        self.steps_per_save = steps_per_save
        self.num_ch = num_ch
        self.data_folder_name = data_folder_name
        self.data_file_prefix = data_file_prefix
        
    def log(self,state):
        datum = Datum(self.num_ch)
        datum.time = datetime.datetime.now().timestamp()
        for i in range(self.num_ch):
            datum.TCA_int[i] = state[i].TCA["int"]
            datum.TCB_int[i] = state[i].TCB["int"]
            datum.TCA_ext[i] = state[i].TCA["ext"]
            datum.TCB_ext[i] = state[i].TCB["ext"]
            datum.TCA_fault[i] = state[i].TCA["fault"]
            datum.TCB_fault[i] = state[i].TCB["fault"]
            datum.output[i] = state[i].output
            datum.setpoint[i] = state[i].command.setpoint
            datum.Kp[i] = state[i].command.Kp
            datum.Ki[i] = state[i].command.Ki
            datum.Kd[i] = state[i].command.Kd
        
        self.data.append(datum)
        if len(self.data) == self.steps_per_save:
            self.save()
            del self.data[:]
            self.data = []
        
    def save(self):
        if self.data:
            dt = datetime.datetime.fromtimestamp(self.data[0].time)
            
            # make today's folder to store data in
            subfolder_name = dt.strftime("%y%m%d")
            this_folder_name = self.data_folder_name + "/" + subfolder_name
            if not os.path.exists(this_folder_name):
                os.makedirs(this_folder_name)
            
            # save file
            this_filename = this_folder_name + "/" + self.data_file_prefix + dt.strftime("_%y%m%d_%H%M%S.pkl")
            fileio.save(self.data,this_filename)
