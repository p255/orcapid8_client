import pickle
import fcntl

def load(filename):
    while True:
        try:
            read_file = open(filename, "rb")
            fcntl.flock(read_file, fcntl.LOCK_EX) #LOCK_NB
            out = pickle.load(read_file)
            #fcntl.flock(read_file, fcntl.LOCK_UN)
            return out
        except Exception as e:
            print("encountered a problem during file load:")
            print(e)
        
    

    
def save(obj,filename):   
    while True:
        try:
            write_file = open(filename, "wb")
            fcntl.flock(write_file, fcntl.LOCK_EX)
            pickle.dump(obj, write_file)
            write_file.flush()
            #fcntl.flock(write_file, fcntl.LOCK_UN)
            break
        except Exception as e:
            print("encountered a problem during file write:")
            print(e)


        # import json
    #def read_json(self,filename):
    #    with open(filename, "r") as read_file:
    #        json.loads(self, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
        
    #def write_json(self,filename):
    #    with open(filename, "w") as write_file:
    #        return json.dump(self, write_file, default=lambda o: o.__dict__, sort_keys=True, indent=4)
    
