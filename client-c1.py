import time
import numpy as np
from rampsequence import RampSequence
from interface import Interface

channel_index = 1 # 1-indexed channel you're controlling
update_delay = 1 # time to wait between updating set point

interface = Interface()

Kp=3
Ki=0.003
Kd=0.000
max_output = 40 # percent

minute=60
hr=3600

seq = RampSequence(channel_index=channel_index)
#add_vertex(t=time in seconds, setpoint=temp in C)
#max output: shouldn't have to change if ramp is slow enough
#sw_enable=True for all steps until last, then False 
seq.add_vertex(t=0,setpoint=55,Kp=Kp,Ki=Ki,Kd=Kd,max_output=max_output,sw_enable=True)
seq.add_vertex(t=5,setpoint=55,Kp=Kp,Ki=Ki,Kd=Kd,max_output=max_output,sw_enable=True)
seq.add_vertex(t=6,setpoint=127,Kp=Kp,Ki=Ki,Kd=Kd,max_output=max_output,sw_enable=True)
seq.add_vertex(t=20*hr,setpoint=127,Kp=Kp,Ki=Ki,Kd=Kd,max_output=max_output,sw_enable=True)
seq.add_vertex(t=20*hr+1,setpoint=105,Kp=Kp,Ki=Ki,Kd=Kd,max_output=max_output,sw_enable=True)
seq.add_vertex(t=30*hr,setpoint=105,Kp=Kp,Ki=Ki,Kd=Kd,max_output=max_output,sw_enable=True)
seq.add_vertex(t=30*hr+1,setpoint=50,Kp=Kp,Ki=Ki,Kd=Kd,max_output=max_output,sw_enable=True)
seq.add_vertex(t=50*hr+1,setpoint=50,Kp=Kp,Ki=Ki,Kd=Kd,max_output=max_output,sw_enable=True)

seq.start() # this starts the timer for the ramp sequence
while True:
    cmd,seq_complete = seq.get_command()
    
    #print(cmd.setpoint)
    interface.send(channel_index, cmd)
    
    if seq_complete:
        break
    
    time.sleep(update_delay)
    