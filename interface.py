import fileio
import state

class Interface:
    def __init__(self,state_filename = "pkl/state_cli_%d.pkl", command_filename = "pkl/cmd_cli_%d.pkl"):
        
        self.state_filename = state_filename
        self.command_filename = command_filename

    def send(self, channel_index, cmd):
        fileio.save(cmd,self.command_filename % channel_index)
        
    def get_state(self, channel_index):
        return fileio.load(self.state_filename % channel_index)
    
    def set_temp(self, channel_index, setpoint):
        cmd = fileio.load(self.command_filename % channel_index)
        cmd.setpoint = setpoint
        #print(cmd.__dict__)
        fileio.save(cmd,self.command_filename % channel_index)
        
    def set_max_output(self, channel_index, max_output):
        cmd = fileio.load(self.command_filename % channel_index)
        cmd.max_output = max_output
        #print(cmd.__dict__)
        fileio.save(cmd,self.command_filename % channel_index)
        
    def set_PID(self, channel_index, Kp, Ki, Kd):
        cmd = fileio.load(self.command_filename % channel_index)
        cmd.Kp = Kp
        cmd.Ki = Ki
        cmd.Kd = Kd
        #print(cmd.__dict__)
        fileio.save(cmd,self.command_filename % channel_index)
        
    def enable(self, channel_index, enabled=True):
        cmd = fileio.load(self.command_filename % channel_index)
        cmd.sw_enable = enabled
        #print(cmd.__dict__)
        fileio.save(cmd,self.command_filename % channel_index)
        
        