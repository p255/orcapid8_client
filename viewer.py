import os
import fileio
import numpy as np
from datetime import datetime as dt
import matplotlib.pyplot as plt
import matplotlib.ticker as tkr
import matplotlib.dates as md

import math

class Viewer:
    def __init__(self, start_datestr, start_timestr, end_datestr, end_timestr, data_dir='data', data_prefix='d_'):
        self.start_datestr = start_datestr
        self.start_timestr = start_timestr
        self.end_datestr = end_datestr
        self.end_timestr = end_timestr
        self.data_dir = data_dir
        self.data_prefix = data_prefix
        
        self.load_data()
        
        

    # note - start_timestr should precede the timestamp of the first datafile of interest
    def load_data(self):#, start_datestr, start_timestr, end_datestr, end_timestr, data_dir='data', data_prefix='d_'):
        # first get all subdirs in data_dir
        all_subdirs = [f.path.split("/")[-1] for f in os.scandir(self.data_dir) if f.is_dir()]

        # collect a pruned list of the subdirs within the date range
        start_date = dt.strptime(self.start_datestr, "%y%m%d")
        end_date = dt.strptime(self.end_datestr, "%y%m%d")
        pruned_subdirs = []
        for subdir in all_subdirs:
            this_date = dt.strptime(subdir, "%y%m%d")
            if this_date >= start_date and this_date <= end_date:
                pruned_subdirs.append(subdir)
        
        # collect a list of files within the time ranges
        pruned_files = []
        start_dt = dt.strptime(self.start_datestr+self.start_timestr, "%y%m%d%H%M%S")
        end_dt = dt.strptime(self.end_datestr+self.end_timestr, "%y%m%d%H%M%S")
        for subdir in pruned_subdirs:
            all_files = [f.path.split("/")[-1] for f in os.scandir(self.data_dir+'/'+subdir) if f.is_file()]
            for file in all_files:
                this_dt = dt.strptime(file, self.data_prefix+"%y%m%d_%H%M%S.pkl")
                if this_dt >= start_dt and this_dt <= end_dt:
                    pruned_files.append(self.data_dir+'/'+subdir+'/'+file)
        
        # build list of data, ordered sequentially
        data = []
        sorted_files = sorted(pruned_files)
        for file in sorted_files:
            chunk = fileio.load(file)
            for d in chunk:
                data.append(d)
        
        # build numpy arrays of data
        self.timestamps = np.zeros(len(data))
        self.TCA_int = np.zeros((8,len(data)))
        self.TCA_ext = np.zeros((8,len(data)))
        self.TCB_int = np.zeros((8,len(data)))
        self.TCB_ext = np.zeros((8,len(data)))
        self.TCA_fault = np.ones((8,len(data)),dtype=np.int8)*-1
        self.TCB_fault = np.ones((8,len(data)),dtype=np.int8)*-1
        self.output = np.zeros((8,len(data)))
        self.setpoint = np.zeros((8,len(data)))
        for i in range(len(data)):
            self.timestamps[i] = data[i].time
            self.TCA_int[:,i] = data[i].TCA_int
            self.TCA_ext[:,i] = data[i].TCA_ext
            self.TCB_int[:,i] = data[i].TCB_int
            self.TCB_ext[:,i] = data[i].TCB_ext
            self.output[:,i] = data[i].output
            try:
                self.setpoint[:,i] = data[i].setpoint
                self.TCA_fault[:,i] = data[i].TCA_fault
                self.TCB_fault[:,i] = data[i].TCB_fault
            except:
                pass
        
    def nullify_fault_data(self,tcdata,faultdata):
        out = np.zeros(tcdata.size)
        for i in range(tcdata.size):
            if faultdata[i] == -1:
                out[i] = tcdata[i]
            else:
                out[i] = np.nan
        return out
        
    def plot(self,channel_list=[1]):
        numplots = len(channel_list)
        plt.ion()
        self.ax = []
        self.ax2 = []
        self.setpoint_lines = []
        self.temperatureA_lines = []
        self.temperatureB_lines = []
        self.output_lines = []
        self.fig = plt.figure()
        for i in range(numplots):
            self.ax.append(self.fig.add_subplot(int(math.ceil(numplots/2)),min(2,numplots),i+1))
            
            # the following lines will format the x axis to display dates rather than timestamps
            dates=[dt.fromtimestamp(ts) for ts in self.timestamps]
            datenums=md.date2num(dates)
            plt.subplots_adjust(bottom=0.2)
            plt.xticks( rotation=25 )
            xfmt = md.DateFormatter('%Y-%m-%d %H:%M:%S')
            self.ax[i].xaxis.set_major_formatter(xfmt)
            
            # to plot vs timestamps, change the x value from dates to self.timestamps and delete the above formatter
            TCA_plotted = self.nullify_fault_data(self.TCA_ext[i],self.TCA_fault[i])
            TCB_plotted = self.nullify_fault_data(self.TCB_ext[i],self.TCB_fault[i])
            line1, = self.ax[i].plot(dates, self.setpoint[i], 'b-')
            line2a, = self.ax[i].plot(dates, TCA_plotted, 'k-')
            line2b, = self.ax[i].plot(dates, TCB_plotted, 'g-')
            
            self.ax2.append(self.ax[i].twinx())
            line3, = self.ax2[i].plot(dates, self.output[i], 'r-')
            self.setpoint_lines.append(line1)
            self.temperatureA_lines.append(line2a)
            self.temperatureB_lines.append(line2b)
            self.output_lines.append(line3)
            
            self.ax[i].set_ylabel('temperature (degC)')
            self.ax2[i].set_ylabel('output (%)')
            self.ax[i].set_xlabel('time (s)')
            self.ax2[i].yaxis.set_major_formatter(tkr.FuncFormatter(lambda y, _: f'{round(y)}') )
            #ax.autofmt_xdate()
            # format dates on x axis
            
            
            ymin = min(np.nanmin(self.setpoint[i]),
                       np.nanmin(TCA_plotted),
                       np.nanmin(TCB_plotted))
            ymax = max(np.nanmax(self.setpoint[i]),
                       np.nanmax(TCA_plotted),
                       np.nanmax(TCB_plotted))
            self.ax[i].set_ylim(ymin-3,
                                ymax+3)
            ymin = min(self.output[i])
            ymax = max(self.output[i])
            self.ax2[i].set_ylim(ymin-3,
                                 ymax+3)
            self.ax[i].set_title('channel %d' % channel_list[i])
            
        self.fig.tight_layout()
        plt.show()
        