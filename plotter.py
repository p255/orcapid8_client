import state
import matplotlib.pyplot as plt
import numpy as np
import datetime
import time
import matplotlib.ticker as tkr
import math

class Plotter:
    def __init__(self,channel_list,state,timesteps=10000):
        self.channel_list = channel_list # 1-indexed list of channels to plot
        self.ax = []
        self.ax2 = []
        self.setpoint_lines = []
        self.temperatureA_lines = []
        self.temperatureB_lines = []
        self.output_lines = []

        self.timesteps = timesteps
        self.numplots = len(channel_list)
        
        self.time0 = time.time()
        self.time = np.zeros(timesteps)
        self.setpoint_data = np.zeros((self.numplots,self.time.size))
        self.temperatureA_data = np.zeros((self.numplots,self.time.size))
        self.temperatureB_data = np.zeros((self.numplots,self.time.size))
        self.output_data = np.zeros((self.numplots,self.time.size))

        self.cur_index = -1
        
        self.update_data(state)

        plt.ion()
        self.fig = plt.figure()
        for i in range(self.numplots):
            self.ax.append(self.fig.add_subplot(int(math.ceil(self.numplots/2)),min(2,self.numplots),i+1))
            
            line1, = self.ax[i].plot(self.time[0:0], self.setpoint_data[i,0:0], 'b-')
            line2a, = self.ax[i].plot(self.time[0:0], self.temperatureA_data[i,0:0], 'k-')
            line2b, = self.ax[i].plot(self.time[0:0], self.temperatureB_data[i,0:0], 'g-')
            
            self.ax2.append(self.ax[i].twinx())
            line3, = self.ax2[i].plot(self.time[0:0], self.output_data[i,0:0], 'r-')
            self.setpoint_lines.append(line1)
            self.temperatureA_lines.append(line2a)
            self.temperatureB_lines.append(line2b)
            self.output_lines.append(line3)
            
            self.ax[i].set_ylabel('temperature (degC)')
            self.ax2[i].set_ylabel('output (%)')
            self.ax[i].set_xlabel('time (s)')
            self.ax2[i].yaxis.set_major_formatter(tkr.FuncFormatter(lambda y, _: f'{round(y)}') )
            #ax.autofmt_xdate()
        self.fig.tight_layout()
        plt.show()
        
    def format_TC_data(self,TC):
        if TC["fault"] == -1:
            return TC["ext"] # if no fault, simply display the correct number
        else:
            return np.nan # otherwise return nan so that plotter ignores this datapoint
        
    def update_data(self,state):
        if self.cur_index == (self.timesteps-1):
            # shift everybody
            self.time[0:self.timesteps-1] = self.time[1:self.timesteps]
            self.setpoint_data[:,0:self.timesteps-1] = self.setpoint_data[:,1:self.timesteps]
            self.temperatureA_data[:,0:self.timesteps-1] = self.temperatureA_data[:,1:self.timesteps]
            self.temperatureB_data[:,0:self.timesteps-1] = self.temperatureB_data[:,1:self.timesteps]
            self.output_data[:,0:self.timesteps-1] = self.output_data[:,1:self.timesteps]
        else:
            self.cur_index += 1
            
        # update data
        self.time[self.cur_index] = time.time()-self.time0
        for i in range(len(self.channel_list)):
            ch = self.channel_list[i]-1 # 0-indexed
            self.setpoint_data[i,self.cur_index] = state[ch].command.setpoint
            self.temperatureA_data[i,self.cur_index] = self.format_TC_data(state[ch].TCA)
            self.temperatureB_data[i,self.cur_index] = self.format_TC_data(state[ch].TCB)
            self.output_data[i,self.cur_index] = state[ch].output
            
    def update(self,state):
        self.update_data(state)
        for i in range(self.numplots):
            self.setpoint_lines[i].set_data(self.time[0:self.cur_index+1],self.setpoint_data[i,0:self.cur_index+1])
            self.temperatureA_lines[i].set_data(self.time[0:self.cur_index+1],self.temperatureA_data[i,0:self.cur_index+1])
            self.temperatureB_lines[i].set_data(self.time[0:self.cur_index+1],self.temperatureB_data[i,0:self.cur_index+1])
            self.output_lines[i].set_data(self.time[0:self.cur_index+1],self.output_data[i,0:self.cur_index+1])
            self.ax[i].set_xlim(self.time[0],self.time[self.cur_index])
            ymin = min(np.nanmin(self.setpoint_data[i,0:self.cur_index+1]),
                       np.nanmin(self.temperatureA_data[i,0:self.cur_index+1]),
                       np.nanmin(self.temperatureB_data[i,0:self.cur_index+1]))
            ymax = max(np.nanmax(self.setpoint_data[i,0:self.cur_index+1]),
                       np.nanmax(self.temperatureA_data[i,0:self.cur_index+1]),
                       np.nanmax(self.temperatureB_data[i,0:self.cur_index+1]))
            self.ax[i].set_ylim(ymin-3,
                                ymax+3)
            ymin = min(self.output_data[i,0:self.cur_index+1])
            ymax = max(self.output_data[i,0:self.cur_index+1])
            self.ax2[i].set_ylim(ymin-3,
                                 ymax+3)
            self.ax[i].set_title('channel %d' % self.channel_list[i])
            
        plt.pause(0.1)