import time
import state


class RampSequence:
    def __init__(self, channel_index):
        self.start_time = 0
        
        self.channel_index = channel_index
        
        self.time = []
        self.setpoint = []
        self.Kp = []
        self.Ki = []
        self.Kd = []
        self.max_output = []
        self.sw_enable = []
        
    def add_vertex(self, t,setpoint,Kp,Ki,Kd,max_output,sw_enable):
        self.time.append(t)
        self.setpoint.append(setpoint)
        self.Kp.append(Kp)
        self.Ki.append(Ki)
        self.Kd.append(Kd)
        self.max_output.append(max_output)
        self.sw_enable.append(sw_enable)
        
    def start(self):
        self.start_time = time.time()
        
    # get_command returns a tuple: (command, boolean indicating sequence completed)
    def get_command(self):
        now = time.time() - self.start_time
        
        # first check if done
        if now>=self.time[-1]:
            # return sw_enable=False command
            return state.ChannelCommand(self.channel_index,self.max_output[-1],self.Kp[-1],
                           self.Ki[-1],self.Kd[-1],self.setpoint[-1],False),True
        
        # not done, so find where we are in the ramp sequence
        i=0
        while now>self.time[i]:
            i+=1
        
        if i == 0:
            # first point, so no interpolation to do
            return state.ChannelCommand(self.channel_index,self.max_output[0],self.Kp[0],
                           self.Ki[0],self.Kd[0],self.setpoint[0],self.sw_enable[0]),False

        # need to interpolate
        fraction = (now-self.time[i-1])/(self.time[i]-self.time[i-1])
        this_setpoint = self.setpoint[i-1]+fraction*(self.setpoint[i]-self.setpoint[i-1])
        return state.ChannelCommand(self.channel_index,self.max_output[i-1],self.Kp[i-1],
                       self.Ki[i-1],self.Kd[i-1],this_setpoint,self.sw_enable[i-1]),False