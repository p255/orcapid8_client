
class ChannelCommand:
    def __init__(self, channel_index, max_output=30, Kp=1.0, Ki=1.0, Kd=1.0,
                 setpoint=30, sw_enable=True):
        # 1-indexed
        self.channel_index = channel_index
        
        self.max_output = max_output
        self.Kp = Kp
        self.Ki = Ki
        self.Kd = Kd
        self.setpoint = setpoint 
        self.sw_enable = sw_enable

class ChannelState:
    def __init__(self, channel_index):
        # 1-indexed
        self.channel_index = channel_index
        
        self.command = ChannelCommand(channel_index) 
        
        # state vars
        self.TCA = {"int": 25, "ext": 30, "fault": -1}
        self.TCB = {"int": 25, "ext": 30, "fault": -1}
        self.hw_enable = True
        self.output = 0
