from interface import Interface
import time


#initialize interface object
interface = Interface()

ch=3
#set_PID(ch, Kp, Ki, Kd)
interface.set_PID(ch,1.5,0.0015,0) # nominal: (ch, 3,0.002,0)

#set_temp(ch, 60) in C
#plot8.py or similar needs to be running
interface.set_temp(ch,15)

interface.set_max_output(ch,40)
#sw_enable is false by default
interface.enable(ch)