import fileio
import state
import time
import numpy as np
import matplotlib.pyplot as plt
from plotter import Plotter
from logger import Logger

state_filename = "pkl/state_cli.pkl"
state_1ch_filename = "pkl/state_cli_%d.pkl"
command_filename = "pkl/cmd_cli.pkl"
command_1ch_filename = "pkl/cmd_cli_%d.pkl"
pause_time = 4
channels = [1,2,3,4, 5, 6,7,8] #1-indexed list of channels to plot



commands = [state.ChannelCommand(i,Kp=1,Ki=0.001,Kd=0.00,setpoint=20,max_output=10,sw_enable=False) for i in range(1,9)]

# create the command files for the individual channels;
#   these will be modified by other client programs
# for c in commands:
#    fileio.save(c,command_1ch_filename % c.channel_index)
    

state = fileio.load(state_filename)
plotter = Plotter(channels,state,timesteps=1000) # we're going to plot channels 1-4
#logger = Logger(steps_per_save=100, data_folder_name="data")
logger = Logger(steps_per_save=100, data_folder_name="../data")

try:
    while True:
        # read in the individual channel commands and compile into one list
        for i in range(len(commands)):
            commands[i] = fileio.load(command_1ch_filename % commands[i].channel_index)
        fileio.save(commands,command_filename) # save the list
        
        # load state of all channels
        state = fileio.load(state_filename)
        
        # save state files for individual channels for use by other client programs 
        for s in state:
            fileio.save(s,state_1ch_filename % s.channel_index)
        
        # update plots
        plotter.update(state)
        
        # update log
        logger.log(state)
        
        time.sleep(pause_time)
        

finally:
    logger.save()
