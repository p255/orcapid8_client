import socket
import sockethelper as sh
import time
import struct

read_file = "pkl/cmd_cli.pkl"
write_file = "pkl/state_cli.pkl"


# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#sock.setsockopt(socket.SOL_SOCKET, socket.SO_BINDTODEVICE, b'enx00e04c680201')#struct.pack("%ds" %(len("eth0")+1,), "enp58s0f1"))

# Connect the socket to the port where the server is listening
#server_address = ('169.254.210.253', 10001)
server_address = ('10.0.0.20', 25522)
print ('connecting to %s port %s' % server_address)
sock.connect(server_address)
print('connected')

while True:
    sock.send(b'W')
    sh.send_file(sock, read_file)
    
    sock.sendall(b'R')
    sh.recv_file(sock, write_file)

    #else:
    #    print ('reconnecting to %s port %s' % server_address)
    #    sock.connect(server_address)
    time.sleep(0.1)


