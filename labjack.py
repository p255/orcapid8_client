from flowmeter import LabJack, FlowMeter


#run this program with thonny
lj = LabJack()
fm1 = FlowMeter(lj, max_flow=200, dac=0, flow_ain=0, ref_ain=2)
fm2 = FlowMeter(lj, max_flow=200, dac=1, flow_ain=1, ref_ain=3)

#to set flow, use:
#fm.set_flow(XX)
#XX=your desired flow rate in sccm
fm1.set_flow(0)
fm2.set_flow(0)
