import socket
from struct import *
import fcntl
        

def recv_file(conn,filename):
    # get next 4 bytes: file size 
    file_len_bytes = conn.recv(4)
    file_len = unpack('I',file_len_bytes)[0]
    
    file_data = conn.recv(file_len)
    
    # write file
    while True:
        try:
            write_file = open(filename, "wb")
            fcntl.flock(write_file, fcntl.LOCK_EX)
            write_file.write(file_data)
            write_file.flush()

            break
        except Exception as e:
            print("encountered a problem during file write:")
            print(e)
            

def send_file(conn,filename):
    # read file
    while True:
        try:
            read_file = open(filename, "rb")
            fcntl.flock(read_file, fcntl.LOCK_EX)
            file_data = read_file.read()
            break
        except Exception as e:
            print("encountered a problem during file load:")
            print(e)
        
        
    msg = b''.join([pack('I',len(file_data)), file_data])
    conn.send(msg)
    